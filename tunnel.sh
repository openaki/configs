#!/bin/bash
if [ $# != 1 ] ; then
	echo "Usage: (sudo) sh $0 {init|start|stop}" 
	exit 1;
fi

VPN_ADDR=209.148.37.34
IFACE=wlp2s0

function getIP(){
	/sbin/ip addr show $1 | grep "inet " | awk '{print $2}' | sed 's:/.*::'       
}

function getGateWay(){
	ip route | grep wlp2 | grep -v default |  awk '{print $1}'
}
function getVPNGateWay(){
	/sbin/route -n | grep -m 1 "$VPN_ADDR" | awk '{print $2}'
}

GW_ADDR=$(getGateWay)  

function start(){
	systemctl restart openswan
	sleep 2    #delay to ensure that IPsec is started before overlaying L2TP

	systemctl restart xl2tpd
        sleep 2
	/usr/sbin/ipsec auto --up L2TP-PSK                        
	/bin/echo "c vpn-connection" > /var/run/xl2tpd/l2tp-control     
	sleep 2    #delay again to make that the PPP connection is up.
}

function stop(){
	/usr/sbin/ipsec auto --down L2TP-PSK
	/bin/echo "d vpn-connection" > /var/run/xl2tpd/l2tp-control
	systemctl stop xl2tpd
	systemctl stop openswan
	
	ip route delete 172.16.0.0/12  via 172.20.190.69
}

$1
exit 0
